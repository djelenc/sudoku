package com.david

import Sudoku._

object RunSudoku extends App {
  val testCase = Vector(
    Vector(0, 0, 2, 0, 8, 0, 6, 0, 0),
    Vector(0, 5, 6, 0, 2, 0, 3, 9, 0),
    Vector(0, 8, 0, 0, 0, 0, 0, 2, 0),
    Vector(0, 0, 5, 1, 4, 9, 7, 0, 0),
    Vector(0, 1, 0, 0, 0, 0, 0, 4, 0),
    Vector(0, 0, 3, 5, 7, 8, 1, 0, 0),
    Vector(0, 3, 0, 0, 0, 0, 0, 7, 0),
    Vector(0, 2, 1, 0, 5, 0, 4, 8, 0),
    Vector(0, 0, 7, 0, 6, 0, 2, 0, 0)).flatten

  val sudoku = new Sudoku(Vector.fill(81)(0))
  println(sudoku)

  println()

  val s = solve(sudoku)
  val solutions = s.take(10).toList

  solutions foreach (elem => println(elem + "\n"))
}