package com.david

object Sudoku {
  val Values = (1 to 9).toStream
  val Indexes = (0 to 8).toList
  val IndexesFull = (0 to 80).toList

  def columnOf(index: Int): Int = index % 9

  def rowOf(index: Int): Int = index / 9

  def squareOf(index: Int): Int = 3 * (rowOf(index) / 3) + (columnOf(index) / 3)

  def generateSuccessors(sudoku: Sudoku): Stream[Sudoku] = {
    val zeroPos = sudoku.data.indexOf(0)

    if (zeroPos == -1)
      Stream.Empty
    else {
      val row = rowOf(zeroPos)
      val column = columnOf(zeroPos)
      val square = squareOf(zeroPos)

      val rowNumbers = sudoku.getRow(row) filter (_ != 0)
      val columnNumbers = sudoku.getColumn(column) filter (_ != 0)
      val squareNumbers = sudoku.getSquare(square) filter (_ != 0)

      val possibleValues = Values filter { e => !rowNumbers.contains(e) && !columnNumbers.contains(e) && !squareNumbers.contains(e) }

      possibleValues map (sudoku.updated(zeroPos, _))
    }
  }

  def solve(sudoku: Sudoku): Stream[Sudoku] = {
    if (sudoku.isComplete) {
      Stream(sudoku)
    } else {
      for {
        newSudoku <- generateSuccessors(sudoku)
        solution <- solve(newSudoku)
      } yield solution
    }
  }
}

class Sudoku(val data: Vector[Int]) {
  import Sudoku.{ Indexes, IndexesFull }

  require(data.length == 81, "Invalid sudoku size.")

  def updated(index: Int, elem: Int): Sudoku = new Sudoku(data.updated(index, elem))

  def getRow(row: Int): List[Int] = Indexes map (e => data(9 * row + e))

  def getColumn(column: Int): List[Int] = Indexes map (e => data(9 * e + column))

  def getSquare(square: Int): List[Int] = IndexesFull.filter(Sudoku.squareOf(_) == square) map (data(_))

  def isComplete(): Boolean = data forall (_ != 0)

  override def toString(): String = {
    def impl(data: Vector[Int]): String = {
      val (current, rest) = data.splitAt(9)

      if (rest.length > 0)
        current.mkString(" ") + "\n" + impl(rest)
      else
        current.mkString(" ")
    }

    impl(data)
  }
}