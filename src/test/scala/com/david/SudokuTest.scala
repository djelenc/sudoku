package com.david

import org.scalatest._

class SudokuTest extends FlatSpec {

  "Sudoku class" should "only accept vectors of length 81" in {
    new Sudoku(Vector.fill(81)(0))
  }

  it should "throw an IllegalArgumentException otherwise" in {
    intercept[IllegalArgumentException] {
      new Sudoku(Vector(1, 2, 3))
    }
  }
}